﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewUserDataFormat
{
//You declared "values" as auto-property. Unity doesn't serialize properties. 
//The next thing is your contructor of your class doesn't work as you never initialize "values".
	public List<string> values = new List<string>();

	public NewUserDataFormat(List<string> l)
	{
		values.AddRange(l);
	}
}
