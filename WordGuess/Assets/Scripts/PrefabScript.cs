﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace WordGuessUser
{
    public class PrefabScript : MonoBehaviour
    {
        public Text _text;
        public Color _FromColor,
                     _ToColor;
        public float _LerpTime = 2.0f;
        // Use this for initialization
        void Start()
        {
            CheckAtStart();
        }
        private void OnEnable()
        {
            CheckAtStart();
        }

        void CheckAtStart()
        {
            gameObject.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            if (_text.text == "?")
            {
                // Debug.Log("TextMatch");
                StartCoroutine("TileEffectUp");
            }
        }
        private IEnumerator TileEffectUp()
        {
            StopCoroutine("TileEffectDown");
            float temptime = _LerpTime;

            while (temptime > 0.1f)
            {
                temptime -= Time.deltaTime;
                gameObject.GetComponent<Image>().color = Color.Lerp(_FromColor, _ToColor, (_LerpTime - temptime) / _LerpTime);
                yield return null;
            }
            gameObject.GetComponent<Image>().color = _ToColor;
            StartCoroutine("TileEffectDown");
            yield return null;
        }

        private IEnumerator TileEffectDown()
        {
            StopCoroutine("TileEffectUp");
            float temptime = _LerpTime;

            while (temptime > 0.1f)
            {
                temptime -= Time.deltaTime;
                gameObject.GetComponent<Image>().color = Color.Lerp(_ToColor, _FromColor,(_LerpTime - temptime) / _LerpTime);
                yield return null;
            }
            gameObject.GetComponent<Image>().color = _FromColor;
            StartCoroutine("TileEffectUp");
            yield return null;
        }

        private void OnDestroy()
        {
            StopCoroutine("TileEffectUp");
            StopCoroutine("TileEffectDown");
        }
    }

}
