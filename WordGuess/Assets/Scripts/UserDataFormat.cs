﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;




[System.Serializable]
public class UserDataFormat
{
	[DataMember]
	public string theValue;
	[DataMember]
	public int uselessKey;
	


	
	public UserDataFormat(string s, int i)
	{
		theValue = s;
		uselessKey = i;
	}

}

[System.Serializable]
public class UserDataArray
{
	public UserDataFormat[] Uarray;

	public UserDataArray(UserDataFormat[] udf)
	{
		Uarray = udf;
	}
}


