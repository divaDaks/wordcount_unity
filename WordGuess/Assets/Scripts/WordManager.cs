﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;


namespace WordGuessUser
{
	public class WordManager : MonoBehaviour
	{
		public static WordManager instance_WM;

		public char _currentGuessLetter = '\0';

		public string _CurrentString = String.Empty;

		public string _CurrentXString = String.Empty;

		public string _CharToTrack = String.Empty;

		public GameObject _parent,
			              _childPrefab;

        public Image _Indicator;
		// Use this for initialization
		void Awake ()
		{
			instance_WM = this;
		}


		string WordPicker()
		{
            string s = String.Empty;
            if(UserGetJson.instance_UGJ.LList.Count>0)
            {
                int i = UnityEngine.Random.Range(0, UserGetJson.instance_UGJ.LList.Count);
                s = UserGetJson.instance_UGJ.LList[i];
                UserGetJson.instance_UGJ.LList.RemoveAt(i);
                _CurrentString = s;
            }
			else
            {
                UserInputHandler.instance_UIH.ReloadScene();
            }
			return s;
		}

		public void WordShower()
		{
			_currentGuessLetter = '\0';
			string s = WordPicker();
            if(String.IsNullOrEmpty(s))
            {
                return;
            }
			int i = UnityEngine.Random.Range(0, s.Length-1);
			_currentGuessLetter = s[i];   //s.Substring(i,1);
			Debug.Log("Word ="+s+" Index ="+i+" Substring ="+_currentGuessLetter);
			_CharToTrack = String.Empty;
			string _display = Replacer(i,_currentGuessLetter);
		    IEnumerator coroutine = WordDestroyer(_display);
			StartCoroutine(coroutine);
			
		}


		public string Replacer(int index,char character)
		{
			StringBuilder sb = new StringBuilder(_CurrentString);
			sb[index] = '?';
			_CurrentXString = sb.ToString();
			_CharToTrack = character.ToString();
			Debug.Log("Show="+_CurrentXString+"~track="+_CharToTrack);
			
			return _CurrentXString;
		}

		public IEnumerator WordDestroyer(string _toPass)
		{
			GameObject []gA = GameObject.FindGameObjectsWithTag("WordPanel");
			for (int x = 0; x < gA.Length; x++)
			{
				Destroy(gA[x]);
				yield return new WaitForSeconds (0.5f);
			}
			
			StopCoroutine("WordSpawnner");
			StartCoroutine("WordSpawnner" , _toPass);
			yield return null;
		}

		public IEnumerator WordSpawnner(string _toShow)
		{
            ResetGridLayout();
            _Indicator.color = Color.white;
            for (int i = 0; i < _toShow.Length; i++)
			{
				GameObject g = Instantiate(_childPrefab);
				g.transform.parent = _parent.transform;
				g.transform.GetChild(0).GetComponent<Text>().text = _toShow[i].ToString();
				yield return new WaitForSeconds(0.5f);

			}
            ResetGridLayout();

            UserInputHandler.instance_UIH._GetThis = _CharToTrack;
			UserInputHandler.instance_UIH._ReadyForInput = true;
			yield return null;
		}

        public void ResetGridLayout()
        {
            _parent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
            _parent.GetComponent<GridLayoutGroup>().spacing = new Vector2(5, 5);
        }
	}

}

