﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using SimpleJson;



namespace WordGuessUser
{
	public class UserGetJson : MonoBehaviour
	{
		
		public static UserGetJson instance_UGJ;
		private static string gameDataProjectFilePath = "/JSON/Words_j.json";

		
        [HideInInspector]
		public List<string> LList = new List<string>();

		// Use this for initialization
		private void Start()
		{
			instance_UGJ = this;
			LoadGameDataAsString();
			
		}

		private void LoadGameDataAsString()
		{
			var filePath = Application.dataPath + gameDataProjectFilePath;

			if (File.Exists(filePath))
			{
				
				var dataAsJson = File.ReadAllText(filePath);
				JDeserialize(dataAsJson as string);
				
			}
			else
			{
				Debug.Log("can't find file : " + filePath);
			}
		}

		void JDeserialize(string jstring)
		{

			
			string JSONToParse = @"{""values"":" + jstring + "}";
			Debug.Log(JSONToParse);
	

			NewUserDataFormat sf = (NewUserDataFormat) JsonUtility.FromJson(JSONToParse, typeof(NewUserDataFormat));
			
			Debug.Log(sf.values.Count);
			foreach (string V in sf.values)
			{
				LList.Add(V);
			}
            WordManager.instance_WM.WordShower();
		}

	}
}

/*-----------------------2
			
//////UserDataFormat gameData = (UserDataFormat)JsonUtility.FromJson(JSONToParse, typeof(UserDataFormat));
//////UserDataFormat[] p = JsonUtility.FromJson<UserDataFormat[]>(JSONToParse);
//////Debug.Log(p.Length);
//////-----------------------
//////UserDataArray player = (UserDataArray)JsonUtility.FromJson<UserDataArray>(JSONToParse);
//////			foreach (UserDataFormat V in p)
//////			{
//////				Debug.Log(V.theValue);
//////			}
//////}
////
//////		private void JsonDeserializer(string jstring)
//////		{
//////			using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(jstring)))
//////			{
//////				// Deserialization from JSON  
////////				DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(BlogSite));
////////				BlogSite bsObj2 = (BlogSite) deserializer.ReadObject(ms);
////////				Response.Write("Name: " + bsObj2.Name); // Name: C-sharpcorner
/////// //UserDataFormat[] p = JsonUtility.FromJson<UserDataFormat[]>(JSONToParse);
////////				Response.Write("Description: " + bsObj2.Description); // Description: Share Knowledge  
//////			}
//////
//////		//Overwrite the values in the existing class instance "playerInstance". Less memory Allocation
//////gameData = (UserDataFormat)JsonUtility.FromJsonOverwrite(jstring, typeof(UserDataFormat));
//////		    UserDataFormat gameData = (UserDataFormat)JsonUtility.FromJson(jstring, typeof(UserDataFormat));
//////			foreach (KeyValuePair<string,int> gd in gameData.KeyWordsFromFile)
//////			{
//////				Debug.Log(gd.Value);
//////			}
//////-----------------------1}
//////string jsonString = "{\"playerId\":\"8484239823\",\"playerLoc\":\"Powai\",\"playerNick\":\"Random Nick\"}";
//////string JSONToParse = "{\"values\":" + jstring + "}";
//////LList.Add(JsonUtility.FromJson<string> (dataAsJson));
//////UserDataFormat dataAsJson = File.ReadAllText (filePath);
//private static string gameDataProjectFilePath = "/StreamingAssets/Words_dictionary.json";*/