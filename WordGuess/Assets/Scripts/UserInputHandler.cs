﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WordGuessUser
{
	public class UserInputHandler : MonoBehaviour
	{
		public static UserInputHandler instance_UIH;
		public bool _ReadyForInput = false;
		private string _Find;
		
		
		void Awake ()
		{
			instance_UIH = this;
		}
		
		public string _GetThis
		{
			get
			{
				return this._Find;
			}
			set
			{
				this._Find = value;
			}
		}
		// Use this for initialization
		
	
		// Update is called once per frame
		void Update () 
		{
			if (_ReadyForInput)
			{
                if(Input.anyKey)
                {
                    if (Input.GetKey(_Find))
                    {
                        Debug.Log("Correct");
                        WordManager.instance_WM._Indicator.color = Color.green;
                        _ReadyForInput = false;
                        WordManager.instance_WM.WordShower();
                    }

                    else
                    {
                        WordManager.instance_WM._Indicator.color = Color.red;
                        //Debug.Log("Incorrect");
                    }
                }
				
			}
			
		}

        public void ReloadScene()
        {
            SceneManager.LoadScene("SampleScene");
        }
	}

}

